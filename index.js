const express = require('express');
const app = express();
const cors = require('cors');
const port = 8000;
const Vigenere = require('caesar-salad').Vigenere;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());

app.post('/encode', (req, res) => {
    const message = {
        password: req.body.password,
        decodedMessage: req.body.decodedMessage,
        encodedMessage: req.body.encodedMessage,
    };

    return res.send({encoded: Vigenere.Cipher(message.password).crypt(message.decodedMessage)});
});

app.post('/decode', (req, res) => {
    const message = {
        password: req.body.password,
        decodedMessage: req.body.decodedMessage,
        encodedMessage: req.body.encodedMessage,
    };

    return res.send({decoded: Vigenere.Decipher(message.password).crypt(message.encodedMessage)});
});

app.listen(port, () => {
    console.log('We are living on ' + port);
});
